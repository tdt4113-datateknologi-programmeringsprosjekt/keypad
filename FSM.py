"""Docstring"""
from inspect import isfunction
from KPCAgent import KPCAgent, wake_up, change_password
from LED_Board import LEDBoard
from Keypad import *


class FSM:
    """Docstring"""
    def __init__(self, endstate, kpc):
        self.state = "s0"
        self.rule_list = []
        self.states = {}
        self.signal = 0
        self.endstate = endstate
        self.kpc = kpc

    def add_rule(self, rule):
        """Docstring"""
        self.rule_list.append(rule)

    def get_next_signal(self):
        """Docstring"""
        return self.kpc.get_next_signal()

    def set_signal(self, signal):
        """Docstring"""
        self.signal = signal

    def run(self):
        """Docstring"""
        while self.state != self.endstate:
            self.signal = self.get_next_signal()
            for rule in self.rule_list:
                if rule.matches(self.state, self.signal):
                    self.fire(rule)
                    break

    def fire(self, rule):
        """Docstring"""
        print("FIRE")
        self.state = rule.state2
        print(self.state)
        try:
            rule.action(self.signal)
        except BaseException:
            rule.action()
        print(self.kpc.cp)
        self.signal = None


class Rule:
    """Docstring"""

    def __init__(self, state1, state2, signal, action):
        self.state1 = state1
        self.state2 = state2
        self.signal = signal
        self.action = action

    def matches(self, state, signal):
        """Docstring"""
        if isfunction(self.signal) and self.state1 == state:
            return self.signal(signal)
        else:  # not isfunction(self.state1):
            return state == self.state1 and self.signal == signal
            # self.fsm.state = self.rule.state2
            # self.rule.action


def waker_up(signal):
    """Docstring"""
    return signal is not None


def is_digit(signal):
    """Docstring"""
    return 48 <= ord(signal) <= 57


def under_six(signal):
    """Docstring"""
    return int(signal) < 6


def main():
    """Docstring"""
    led = LEDBoard()
    keypad = Keypad()
    keypad.setup()
    kpc = KPCAgent(keypad, led, "password.txt", 0)
    fsm = FSM("s8", kpc)

    rule_0 = Rule("s0", "s1", waker_up, wake_up)
    rule_1 = Rule("s1", "s0", "#", kpc.reset_try)
    rule_2 = Rule("s1", "s1", is_digit, kpc.add_number)
    rule_3 = Rule("s1", "s2", "*", kpc.verify_login)
    rule_4 = Rule("s2", "s0", "N", kpc.logout)
    rule_5 = Rule("s2", "s3", "Y", kpc.activate)

    rule_6 = Rule("s3", "s4", "*", change_password)
    rule_7 = Rule("s4", "s4", is_digit, kpc.add_number)
    rule_8 = Rule("s4", "s3", "*", kpc.validate_passcode_change)

    rule_9 = Rule("s3", "s0", "#", kpc.logout)
    rule_10 = Rule("s3", "s6", under_six, kpc.led_to_light)
    rule_11 = Rule("s6", "s6", is_digit, kpc.add_number)
    rule_12 = Rule("s6", "s3", "*", kpc.light_one_led)

    fsm.add_rule(rule_1)
    fsm.add_rule(rule_2)
    fsm.add_rule(rule_0)
    fsm.add_rule(rule_3)
    fsm.add_rule(rule_4)
    fsm.add_rule(rule_5)
    fsm.add_rule(rule_6)
    fsm.add_rule(rule_7)
    fsm.add_rule(rule_8)
    fsm.add_rule(rule_9)
    fsm.add_rule(rule_10)
    fsm.add_rule(rule_11)
    fsm.add_rule(rule_12)

    fsm.run()


if __name__ == '__main__':
    main()
