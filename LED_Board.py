"""Docstring"""

import time  # Import the sleep function from the time module
from GPIOSimulator_v5 import GPIOSimulator, keypad_col_pins, PIN_CHARLIEPLEXING_0,\
    PIN_CHARLIEPLEXING_1, PIN_CHARLIEPLEXING_2

GPIO = GPIOSimulator()


class LEDBoard:
    """Controls the LED-Board"""

    def __init__(self):
        self.pins = keypad_col_pins
        self.pin_led_states = {
            'LED0': ('HIGH', 'LOW', 'IN'),
            'LED1': ('LOW', 'HIGH', 'IN'),
            'LED2': ('IN', 'HIGH', 'LOW'),
            'LED3': ('IN', 'LOW', 'HIGH'),
            'LED4': ('HIGH', 'IN', 'LOW'),
            'LED5': ('LOW', 'IN', 'HIGH'),
            'NONE': ('IN', 'IN', 'IN')
        }

    @staticmethod
    def set_pin(pin_index, pin_state):
        """
        :param pin_index: pin; pin, outpin
        :param pin_state: state; IN, OUT, HIGH, LOW
        """
        if pin_state == 'LOW':
            GPIO.setup(pin_index, GPIO.OUT, GPIO.LOW)
        elif pin_state == 'HIGH':
            GPIO.setup(pin_index, GPIO.OUT, GPIO.HIGH)
        elif pin_state == 'IN':
            GPIO.setup(pin_index, GPIO.IN, GPIO.LOW)

    def light_led(self, led_number):
        """Turn on the repective LED"""
        setting = self.pin_led_states.get(led_number)
        self.set_pin(PIN_CHARLIEPLEXING_0, setting[0])
        self.set_pin(PIN_CHARLIEPLEXING_1, setting[1])
        self.set_pin(PIN_CHARLIEPLEXING_2, setting[2])
        GPIO.show_leds_states()

    def flash_all_leds(self, k):
        """Flash all 6 LEDs on and off for k seconds, where k is an argument of
         the method."""
        flash_frq = 0.3
        debug = False

        print("Flashing all leds")
        start = time.time()
        while True:
            if time.time() - start >= k:
                print('Finish', time.time() - start)
                break

            start_flash = time.time()
            c_rounds = 1

            # Light is turned on for a second
            print(time.time() - start, 'Lights on')
            while True:
                if time.time() - start_flash >= flash_frq:
                    break
                print(
                    c_rounds,
                    '--------------------------------------------------') if debug else 0
                for led_name in (
                    'LED0',
                    'LED1',
                    'LED2',
                    'LED3',
                    'LED4',
                        'LED5'):
                    self.light_led(led_name)
                    GPIO.show_leds_states() if debug else 0  # Shows that the leds iterate
                c_rounds += 1
                time.sleep(0.030)  # Light dim, 33hz

            # After a second with the lights on, turn them off for a second
            self.turnoff_leds()
            print(time.time() - start, 'Lights off')
            GPIO.show_leds_states() if debug else 0  # Shows that the lights turned off
            time.sleep(flash_frq)

    def turnoff_leds(self):
        """Helping method to turn off all LEDs"""
        self.light_led('NONE')


    def twinkle_all_leds(self):
        """Turn all LEDs on and off in sequence for k seconds, where k is an
        # argument of the method."""
        print("twinkling all leds")

        timeend = time.time() + 5  # current time + 5 sec

        while time.time() < timeend:
            for i in range(0, 6):
                self.light_led('LED' + str(i))
                time.sleep(0.1)
                #GPIO.show_leds_states()
                self.turnoff_leds()


def main():
    """Used for testing purposes"""
    test = LEDBoard()
    test.twinkle_all_leds()


if __name__ == '__main__':
    main()
