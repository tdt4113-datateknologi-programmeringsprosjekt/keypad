"""keypad"""

import time
# import pynput
from GPIOSimulator_v5 import GPIOSimulator, keypad_row_pins, keypad_col_pins, PIN_KEYPAD_ROW_0, PIN_KEYPAD_ROW_1, \
    PIN_KEYPAD_ROW_2, PIN_KEYPAD_ROW_3, PIN_KEYPAD_COL_0, PIN_KEYPAD_COL_1, PIN_KEYPAD_COL_2
GPIO = GPIOSimulator()


class Keypad:
    """Keypad"""

    def __init__(self):
        self.coord = {(3, 7): '1',
                      (3, 8): '2',
                      (3, 9): '3',
                      (4, 7): '4',
                      (4, 8): '5',
                      (4, 9): '6',
                      (5, 7): '7',
                      (5, 8): '8',
                      (5, 9): '9',
                      (6, 7): '*',
                      (6, 8): '0',
                      (6, 9): '#'}

    def setup(self):
        """Initialize the row pins as outputs and the column pins as inputs"""
        GPIO.setup(PIN_KEYPAD_ROW_0, GPIO.OUT)
        GPIO.setup(PIN_KEYPAD_ROW_1, GPIO.OUT)
        GPIO.setup(PIN_KEYPAD_ROW_2, GPIO.OUT)
        GPIO.setup(PIN_KEYPAD_ROW_3, GPIO.OUT)
        GPIO.setup(PIN_KEYPAD_COL_0, GPIO.IN, state=GPIO.LOW)
        GPIO.setup(PIN_KEYPAD_COL_1, GPIO.IN, state=GPIO.LOW)
        GPIO.setup(PIN_KEYPAD_COL_2, GPIO.IN, state=GPIO.LOW)

    def do_polling(self):
        """Use nested loops to determine the key currently being
            pressed on the keypad."""
        # Outer, loops cycles through the rows
        pressed_key = None
        for row_pin in keypad_row_pins:
            GPIO.output(row_pin, GPIO.HIGH)
            # Inner, loops cycles through the cols
            for col_pin in keypad_col_pins:
                handler = 0  # A switch/ listener to get rid of multiple "clicks"
                # Read as input, one at a time
                if GPIO.input(col_pin) == GPIO.HIGH:
                    handler = 1
                    # press_start = time.time()
                    # if time.time() - press_start
                    pressed_key = self.coord[row_pin, col_pin]
                    break
                if GPIO.input(col_pin) == GPIO.LOW and handler == 1:
                    # press_duration = time.time() - press_start
                    handler = 0
                # Reset col pin?
            GPIO.output(row_pin, GPIO.LOW)  # Reset the row pin
        return pressed_key

    def get_next_signal(self):
        """This is the main interface between the agent and the keypad. It should
            initiate repeated calls to do polling until a key press is detected."""
        print("Waiting for button press...")
        key = None
        while key is None:
            key = self.do_polling()
        time.sleep(0.08)  # Polling frequency delay = 1 ms
        return key


def main():
    """Docstring"""
    keypad = Keypad()
    keypad.setup()
    print(keypad.get_next_signal())


if __name__ == '__main__':
    main()
