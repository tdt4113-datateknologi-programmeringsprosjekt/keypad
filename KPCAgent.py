"""Docstring"""
import time
from GPIOSimulator_v5 import *
GPIO = GPIOSimulator()


def change_password():
    """Docstring"""
    print("Enter new password")


def wake_up():
    """Docstring"""
    print("Welcome")


def do_action(signal):
    """Docstring"""
    print(signal)
    # self.flash_leds(0.5)


class KPCAgent:
    """Docstring"""

    def __init__(self, keypad, led_board, path_name, override_signal):
        self.keypad = keypad
        self.led_board = led_board
        self.path_name = path_name  # To the password
        self.override_signal = override_signal
        self.cp = ""
        self.lid = -1

    def reset_passcode_entry(self):
        """Clear passcode buffer and initiate a 'power up' lightning sequence on the LED board """
        self.cp = ""
        # self.flash_leds(0.3)

    def get_next_signal(self):
        """Return the override-signal"""
        if self.override_signal == "Y":
            signal = self.override_signal
            self.override_signal = None
        elif self.override_signal == "N":
            signal = self.override_signal
            self.override_signal = None
        else:
            signal = self.keypad.get_next_signal()
        return signal

    def verify_login(self):
        """Check that the password just entered via the keypad matches that in the password file"""
        file = open(self.path_name)
        password = file.read()
        file.close()
        if str(self.cp) == password:
            self.override_signal = 'Y'
        else:
            self.override_signal = 'N'

    def validate_passcode_change(self):
        """Check that the new password is legal"""
        if len(str(self.cp)) >= 4:
            file = open(self.path_name, "r+")
            file.write(str(self.cp))
            file.close()
            self.twinkle_leds()
        else:
            self.flash_leds()

    def light_one_led(self):  # lid ex. 'LED3'
        """Docstring"""
        self.led_board.light_led("LED" + str(self.lid))
        time.sleep(int(self.cp))
        self.led_board.turnoff_leds()
        self.reset_passcode_entry()

    def flash_leds(self):
        """Docstring"""
        self.led_board.flash_all_leds(2)

    def twinkle_leds(self):
        """Docstring"""
        self.led_board.twinkle_all_leds()

    def exit_action(self):
        """Docstring"""
        self.twinkle_leds()

    def add_number(self, signal):
        """Docstring"""
        self.cp += signal

    def reset_try(self):
        """Docstring"""
        self.reset_passcode_entry()
        print("Enter code again")

    def logout(self):
        """Docstring"""
        self.reset_passcode_entry()
        print("Logging out")
        self.flash_leds()
        time.sleep(0.5)

    def activate(self):
        """Docstring"""
        print("You are now in")
        self.reset_passcode_entry()
        self.twinkle_leds()

    def led_to_light(self, signal):
        """Docstring"""
        self.lid = signal
        print(f"pin to light: {self.lid}")
